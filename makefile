all: trace plot

trace:
	./tracehost.sh < hosts

plot:
	./plothost.sh | fdp -T svg > public/hosts.svg

timestamp:
	@echo "https://gitlab.com/deanturpin/tracehost $(shell date) $(shell cat public/index.html)" > public/index.html

fox:
	firefox public/index.html

clean:
	rm -f public/*.txt
